Installation Order:
WICO - Windsong Immersive Chracter Overhaul
	Follow current guide instructions.
	
WICO - Striped BSA
	Follow current guide instructions.
	
Improved Bards - Special
	Improved Bards - Special Edition USSEP v1.2-7956-1-2.rar
	Delete the following:
	
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\0001A670.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\0001AA63.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\0001328F.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\000132AD.NIF

	* textures\actors\character\FaceGenData\FaceTint\Skyrim\0001A670.dds
	* textures\actors\character\FaceGenData\FaceTint\IB - All-in-One.esp\0001A670.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\0001AA63.dds
	* textures\actors\character\FaceGenData\FaceTint\IB - All-in-One.esp\0001AA63.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\0001328F.dds
	* textures\actors\character\FaceGenData\FaceTint\IB - All-in-One.esp\0001328F.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\000132AD.dds
	* textures\actors\character\FaceGenData\FaceTint\IB - All-in-One.esp\000132AD.dds
	* IB - All-in-One.esp
	
Makeovers by Mizzog - The Companions
	MbM_Companions_ME_SSE_bsa-5932-ME-1-0.7z
	Delete the following:
	* MbM_Companions_ME_SSE.esp

Followers Hirelings and Housecarls SSE
	Follow current guide instructions.

Followers Hirelings and Housecarls SSE - Mizz Edition Warpaints
	Follow current guide instructions.

Metalsabers Beautiful Orcs of Skyrim
	Follow current guide instructions.

The Ordinary Women SSE
	Follow current guide instructions.
	Note: The Ordinary Women SSE - USSEP Compatibility is merged into NPC Retexture Merge SSE.
	Delete the following:

	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\00013297.NIF

	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\00013297.dds

Pandorable's NPCs
	Pandorable's NPCs SE-19012-1-3.rar
	
	Delete the following:
	
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\0001325C.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\0001C187.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\00013BB0.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\00013265.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\00013269.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\00013271.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\0001C197.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\0001C184.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\0001412D.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\0001412C.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\00014123.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\0001336F.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim,esm\00013B9B.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\0001329F.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\00013476.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\00014121.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\0001337C.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\0001C189.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\000132AE.NIF

	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\0001325C.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\0001C187.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\00013BB0.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\00013265.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\00013269.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\00013271.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\0001C197.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\0001C184.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\0001412D.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\0001412C.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\00014123.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\0001336F.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\00013B9B.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\0001329F.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\00013476.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\00014121.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\0001337C.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\0001C189.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\000132AE.dds
	* PAN_NPCs.esp
	
Kalilies NPCs	
	Kalilies NPCs - Kalilies NPCs - UNP-30247-2-0-1575732483.7z
	Delete the following:

	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\000CAB2F.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\00013273.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\00013654.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\00013BA4.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\00013369.NIF

	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\000CAB2F.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\00013273.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\00013654.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\00013BA4.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\00013369.dds
	* KaliliesNPCs.esp
	
Fresh Faces - SSE
	Follow current guide instructions.
	Delete the following:

	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\0001C196.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\0001339E.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\00048C2F.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\00047CAD.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\0001C1A0.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\00014129.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\000136BC.NIF

	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\0001C196.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\0001339E.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\00048C2F.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\00047CAD.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\0001C1A0.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\00014129.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\000136BC.dds

Pandorable's NPCs - Dawnguard SE - USSEP + Grey face fix SE
	Pandorable's NPCs - Dawnguard SE - USSEP-24135-1-2-1552598434.rar
	Pandorable's NPCs - Dawnguard SE - grey face fix-24135-1-1-1552519919.rar
	Delete the following:
	
	* Meshes\Actors\Character\FaceGenData\FaceGeom\dawnguard.esm\0000336E.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\dawnguard.esm\00003788.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\dawnguard.esm\0000336D.NIF

	* textures\actors\character\FaceGenData\FaceTint\Dawnguard.esm\0000336E.dds
	* textures\actors\character\FaceGenData\FaceTint\Dawnguard.esm\00003788.dds
	* textures\actors\character\FaceGenData\FaceTint\Dawnguard.esm\0000336D.dds
	* PAN_NPCs_DG.esp
	
Pandorables NPCs - Dragonborn
	Pandorable's NPCs - Dragonborn SE
	Choose the following Options from the fomod:

	*Baldor Iron Spear - Alternative
	* Bujold The Unworthy - Altnerative
	* Fanari Strong Voive - Alternatavie
	* Halbarn Iron Fur - Alternative
	* Hilund - Alternative
	* Elder Othreloth - Default
	* Ralis Sedarys - Alternative
	* Storn Crag Strider - Deafult
	* Teldrym Sero - Default
	* Captain Veleth - Alternative
	* Wulf Wild Blood - Alternative
	* USSEP Yes
Delete the following.
	* PAN_NPCs_DB.esp

Bijin Warmaidens SE
	Follow current guide instructions.
	Delete the following.

	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\00019959.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\00028AD0.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\0001C196.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\00048C2F.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\0001336B.NIF

	* textures\actors\character\Aranea
	* textures\actors\character\Borgakh
	* textures\actors\character\Brelyna
	* textures\actors\character\Illia
	* textures\actors\character\Mjoll

	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\00019959.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\00028AD0.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\0001C196.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\00048C2F.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\0001336B.dds
	
Bijin Wives SE
	Follow current guide instructions.

Bijin NPCs SE
	Follow current guide instructions.
	Delete the following:

	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\00013364.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\0001336A.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\00013297.NIF

	* textures\actors\character\Idgrod the younger
	* textures\actors\character\Ingun
	* textures\actors\character\Lisette
	* textures\actors\character\Maven

	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\00013364.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\0001336A.dds
	* textures\actors\character\FaceGenData\FaceTint\Skyrim.esm\00013297.dds

	
Bijin All in One 2019
	Bijin All in One 2019 is merged into NPC Retexture Merge SSE.
	Bijin Optimized Mesh Resource - UNP-11-4-1-0-1551957067.7z
	Delete the following:
	
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\00019959.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\00028AD0.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\0001C196.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\00048C2F.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\0001336B.NIF
		* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\00013364.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\0001336A.NIF
	* Meshes\Actors\Character\FaceGenData\FaceGeom\Skyrim.esm\00013297.NIF
	
eeekie's Enhanced NPCs
Install all main main files
Delete the following:

* Alvor Replacement V2.esp
* eeekie Elisif replacer.esp
* eeekie's Balimund Replacer.esp
* eeekie's brynjolf.esp
* eeekie's Jon Battle Born.esp
* eeekie's Laila Law Giver.esp
* eeekie's Young Idgrod replacer.esp
* Idgrod Replacer.esp
* TolfdirV2.esp
	
Changelog:

16\12\19
Added:
* eeekie's Enhanced NPCs

Removed:
* None

Updated:
* Consistency Patch
* Merge
* Conflict Resolution  

Lexy 30\11\19
Added:
Pandorables NPCs - Dragonborn

Removed:
* None

Updated:
* Merge
* CR


Lexy: 26\11\19

Removed:
* The Men of Winter

Updated:
* Bijin All in One instrcutions for Aranea Ienith removal
* Kalilies NPCs instrcutions for Narri removal
* Consistency Patch
* Merge
* CR

Men Of Winter FOMOD Selections Pick The Following NPCs:
* Brynjolf
* Cicero
* Derkeethus
* Enthir
* Falk Firebeard
* Jon Battle-Born
* Madanach
* Runil
* Ysgramor

* Updated Pandorables NPCs - Dawnguard instrustions
* Updaated Bijin All in One
* Updaated Consistency Patch
* Updated Merge.

* add for The Ordinary Women
* add instrutions for The Men of Winter
* Updated Merge 
* Updated Consistency Patch

* Diana's interpreted destructions.